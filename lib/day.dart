import 'package:flutter/material.dart';
import 'package:heat_app/form/feelingForm.dart';
import 'package:heat_app/form/weightForm.dart';
import 'package:heat_app/progress.dart';
import 'package:heat_app/service/date.service.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class Day extends StatelessWidget {
  final DateTime date;
  final bool keyboardIsOpen;

  Day(this.date, this.keyboardIsOpen);

  @override
  Widget build(BuildContext context) {
    DateState dateState = Provider.of<DateState>(context);

    return Stack(
      children: [
        keyboardIsOpen
            ? Container()
            : Align(
                alignment: Alignment.topCenter,
                child: new ConstrainedBox(
                  constraints: new BoxConstraints(
                    maxHeight: 100.0,
                  ),
                  child: Progress(),
                )),
        Container(
          margin: EdgeInsets.fromLTRB(0, keyboardIsOpen ? 0 : 100, 0, 0),
          color: Color.fromRGBO(48, 48, 48, 1.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                child: Ink(
                  decoration: ShapeDecoration(
                    color: Colors.grey,
                    shape: CircleBorder(),
                  ),
                  child: IconButton(
                    icon: Icon(Icons.arrow_back),
                    color: Colors.white,
                    onPressed: () {
                      dateState.setPreviousDay();
                    },
                  ),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  GestureDetector(
                      onTap: () {
                        showDatePicker(
                          context: context,
                          initialDate: date,
                          firstDate: DateTime(2018),
                          lastDate: DateTime.now(),
                          builder: (BuildContext context, Widget child) {
                            return Theme(
                              data: ThemeData.dark(),
                              child: child,
                            );
                          },
                        ).then((selectedDate) {
                          if (selectedDate != null) {
                            dateState.setSelectedDay(selectedDate);
                          }
                        });
                      },
                      child: Column(children: <Widget>[
                        Text(
                          dateService.getDateName(date),
                          textScaleFactor: 2,
                        ),
                        Text(
                          new DateFormat('d. LLLL y').format(date),
                          textScaleFactor: 1.25,
                        ),
                      ])),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 20),
                    child: FeelingForm(date),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 20),
                    child: WeightForm(date),
                  ),
                ],
              ),
              Container(
                child: Ink(
                  decoration: ShapeDecoration(
                    color: Colors.grey,
                    shape: CircleBorder(),
                  ),
                  child: IconButton(
                    icon: Icon(Icons.arrow_forward),
                    color: Colors.white,
                    onPressed: dateService.isToday(date)
                        ? null
                        : () {
                            dateState.setNextDay();
                          },
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
