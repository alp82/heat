import 'package:flutter/material.dart';
import 'package:heat_app/drawer.dart';
import 'package:heat_app/service/date.service.dart';
import 'package:heat_app/service/feeling.service.dart';
import 'package:heat_app/service/weight.service.dart';
import 'package:provider/provider.dart';

import 'day.dart';

class Application extends StatefulWidget {
  @override
  ApplicationState createState() {
    return ApplicationState();
  }
}

class ApplicationState extends State<Application> with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    weightService.unsubscribeFromDB();
    feelingService.unsubscribeFromDB();
    super.dispose();
  }

  var keyboardIsOpen = false;

  @override
  void didChangeMetrics() {
    final value = MediaQuery.of(context).viewInsets.bottom;
    if (value > 0) {
      if (keyboardIsOpen) {
        _onKeyboardChanged(false);
      }
      keyboardIsOpen = false;
    } else {
      keyboardIsOpen = true;
      _onKeyboardChanged(true);
    }
  }

  _onKeyboardChanged(bool isVisible) {
    if (isVisible) {
      print("KEYBOARD VISIBLE");
    } else {
      print("KEYBOARD HIDDEN");
    }
  }

  @override
  Widget build(BuildContext context) {
    weightService.subscribeToDB(context);
    feelingService.subscribeToDB(context);

    DateState dateState = Provider.of<DateState>(context);

    return Scaffold(
      appBar: AppBar(
          title: Row(children: <Widget>[
        Container(
          margin: EdgeInsets.fromLTRB(0, 0, 8, 0),
          child: Image(
            image: AssetImage('assets/logo.png'),
            width: 24,
          ),
        ),
        Text(
          'Heat',
          textScaleFactor: 1.5,
        ),
      ])),
      body: Container(
          constraints: BoxConstraints.expand(),
          child: GestureDetector(
//            onScaleUpdate: (details) {
//              if (details.scale > 1.1) {
//                print(details.scale);
//              } else if (details.scale < 0.9) {
//                print(details.scale);
//              }
//            },
//            onPanUpdate: (details) {
//              print(details.delta.direction);
//              print(details.delta.distance);
//              print(details.delta.distanceSquared);
//              print(details.delta.dx);
//              print(details.delta.dy);
//              if (details.delta.dx > 8 &&
//                  !dateService.isToday(dateState.selectedDay)) {
////            dateState.setNextDay();
//              } else if (details.delta.dx < 8) {
////            dateState.setPreviousDay();
//              }
//            },
            child: Day(dateState.selectedDay, keyboardIsOpen),
          )),
      drawer: AppDrawer(),
    );
  }
}
