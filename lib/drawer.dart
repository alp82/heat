import 'package:flutter/material.dart';
import 'package:heat_app/service/auth.service.dart';
import 'package:heat_app/service/date.service.dart';
import 'package:provider/provider.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    DateState dateState = Provider.of<DateState>(context);
    Profile profile = Provider.of<AuthState>(context).profile;

    return Drawer(
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Heat',
                  textScaleFactor: 2,
                ),
                Text('Simple Health Tracker'),
                Container(
                    margin: EdgeInsets.all(12),
                    child: ClipOval(
                      child: Image.network(
                        profile.photoUrl,
                        width: 48,
                      ),
                    )),
              ],
            ),
            decoration: BoxDecoration(
              color: Colors.black54,
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(0, 0, 0, 16),
            child: RaisedButton(
              child: Text(
                'Jump to today',
                textScaleFactor: 1.5,
              ),
              onPressed: () {
                dateState.setToday();
                Navigator.of(context).pop(); // close drawer
              },
            ),
          ),
          Center(
            child: Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
              LogoutButton(),
            ]),
          )
        ],
      ),
    );
  }
}

class LogoutButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
        onPressed: () => authService.signOut(context),
        color: Colors.white,
        textColor: Colors.black,
        child: Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
          Container(
            margin: EdgeInsets.fromLTRB(0, 0, 8, 0),
            child: Icon(Icons.arrow_back),
          ),
          Text(
            'Sign Out',
            textScaleFactor: 1.5,
          ),
        ]));
  }
}
