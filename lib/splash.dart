import 'package:flutter/material.dart';
import 'package:heat_app/service/auth.service.dart';
import 'package:provider/provider.dart';

class LoginSplash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    bool loading = Provider.of<AuthState>(context).loading;

    Widget image;

    if (loading) {
      image = Container(
        margin: EdgeInsets.all(30),
        child: CircularProgressIndicator(),
      );
    } else {
      image = Image(
        image: AssetImage('assets/logo.png'),
        width: 96,
      );
    }

    return Scaffold(
        body: Center(
            child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          'Heat',
          textScaleFactor: 3,
        ),
        Container(
          margin: EdgeInsets.all(20),
          child: image,
        ),
        LoginButton(loading: loading),
      ],
    )));
  }
}

class LoginButton extends StatelessWidget {
  final bool loading;

  const LoginButton({this.loading}) : super();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
        onPressed: loading ? () => {} : () => authService.googleSignIn(context),
        color: Colors.black38,
        child: Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
          Container(
            margin: EdgeInsets.fromLTRB(0, 0, 8, 0),
            child: Image(
              image: AssetImage('assets/google-logo.png'),
              width: 16,
            ),
          ),
          Text(
            'Sign In with Google',
            textScaleFactor: 1.5,
          ),
        ]));
  }
}
