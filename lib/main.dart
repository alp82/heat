import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:heat_app/app.dart';
import 'package:heat_app/service/auth.service.dart';
import 'package:heat_app/service/date.service.dart';
import 'package:heat_app/service/feeling.service.dart';
import 'package:heat_app/service/weight.service.dart';
import 'package:heat_app/splash.dart';
import 'package:provider/provider.dart';

void main() => runApp(Main());

class Main extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return MultiProvider(
        providers: [
          ChangeNotifierProvider<DateState>(
            builder: (context) => DateState(),
          ),
          ChangeNotifierProvider<AuthState>(
            builder: (context) => AuthState(),
          ),
          ChangeNotifierProvider<FeelingState>(
            builder: (context) => FeelingState(),
          ),
          ChangeNotifierProvider<WeightState>(
            builder: (context) => WeightState(),
          ),
        ],
        child: MaterialApp(
            title: 'Heat',
            theme: ThemeData(
              brightness: Brightness.dark,
              primarySwatch: Colors.deepOrange,
            ),
            home: AuthSwitch()));
  }
}

class AuthSwitch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    FirebaseUser user = Provider.of<AuthState>(context).user;

    if (user == null) {
      return LoginSplash();
    } else {
      return Application();
    }
  }
}
