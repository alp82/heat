import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:heat_app/service/date.service.dart';
import 'package:heat_app/service/feeling.service.dart';
import 'package:heat_app/service/weight.service.dart';
import 'package:provider/provider.dart';

class Progress extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    DateState dateState = Provider.of<DateState>(context);
    WeightState weightState = Provider.of<WeightState>(context);
    FeelingState feelingState = Provider.of<FeelingState>(context);

    List<Weight> weightsForWeek =
        weightState.weights.where((weight) => weight.date.difference(dateState.selectedDay).inDays <= 3).toList();
    List<Feeling> feelingsForWeek =
        feelingState.feelings.where((feeling) => feeling.date.difference(dateState.selectedDay).inDays <= 3).toList();

    // weight progress as line chart
    var weightSeries = new charts.Series<Weight, DateTime>(
      id: 'Weight Progress',
      data: weightsForWeek,
      measureLowerBoundFn: (Weight weight, _) => weight.amount - 4,
      measureUpperBoundFn: (Weight weight, _) => weight.amount + 4,
      domainFn: (Weight weight, _) => weight.date,
      measureFn: (Weight weight, _) => weight.amount,
      colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
    );

    var feelingSeries = new charts.Series<Feeling, DateTime>(
      id: 'Feeling progress',
      data: feelingsForWeek,
      domainFn: (Feeling feeling, _) => feeling.date,
      domainLowerBoundFn: (Feeling feeling, _) => feeling.date.subtract(new Duration(hours: 12)),
      domainUpperBoundFn: (Feeling feeling, _) => feeling.date.add(new Duration(hours: 12)),
      measureFn: (_, __) => null,
      colorFn: (Feeling feeling, _) => feelingService.getChartColor(feeling, dateState.selectedDay),
    )
      ..setAttribute(charts.rendererIdKey, 'feelingsAnnotation')
      ..setAttribute(charts.boundsLineRadiusPxKey, 5.5);

//    var feelingSegments = feelingState.feelings.map((feeling) {
//      return new charts.RangeAnnotationSegment(feeling.date.subtract(new Duration(hours: 12)),
//          feeling.date.add(new Duration(hours: 12)), charts.RangeAnnotationAxisType.domain,
//          color: feelingService.getChartColor(feeling, dateState.selectedDay));
//    }).toList();

    return new charts.TimeSeriesChart(
      [weightSeries, feelingSeries],
//      behaviors: [new charts.RangeAnnotation(feelingSegments)],
      animate: false,
      domainAxis: new charts.DateTimeAxisSpec(
          renderSpec: new charts.SmallTickRendererSpec(
              labelStyle: new charts.TextStyleSpec(color: charts.MaterialPalette.gray.shade500))),
      primaryMeasureAxis: new charts.NumericAxisSpec(
          renderSpec: new charts.SmallTickRendererSpec(
              labelStyle: new charts.TextStyleSpec(color: charts.MaterialPalette.gray.shade500)),
          tickProviderSpec: new charts.BasicNumericTickProviderSpec(zeroBound: false)),
      customSeriesRenderers: [
        new charts.SymbolAnnotationRendererConfig(
          customRendererId: 'feelingsAnnotation',
          showSeparatorLines: false,
        )
      ],
      dateTimeFactory: const charts.LocalDateTimeFactory(),
    );
  }
}
