import 'package:charts_flutter/flutter.dart' as charts;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'auth.service.dart';

enum Mood { happy, neutral, sad }

class Feeling {
  String userUid;
  Mood mood;
  DateTime date;
  int increment;
  DateTime updated;
}

class FeelingState with ChangeNotifier {
  bool loading = false;
  List<Feeling> feelings = [];

  void setLoading(bool _loading) {
    loading = _loading;
    notifyListeners();
  }

  void setFeelings(List<Feeling> _feelings) {
    _feelings = _feelings;
    notifyListeners();
  }

  void addFeeling(Feeling _feeling) {
    feelings.add(_feeling);
    notifyListeners();
  }

  void updateFeeling(Feeling _feeling) {
    int feelingIndex = findFeelingIndex(_feeling);
    if (feelingIndex >= 0) {
      feelings[feelingIndex] = _feeling;
      notifyListeners();
    }
  }

  void removeFeeling(Feeling _feeling) {
    int feelingIndex = findFeelingIndex(_feeling);
    if (feelingIndex >= 0) {
      feelings.removeAt(feelingIndex);
      notifyListeners();
    }
  }

  int findFeelingIndex(Feeling _feeling) {
    return feelings.indexWhere((feeling) =>
        feeling.date == _feeling.date &&
        feeling.increment == _feeling.increment &&
        feeling.userUid == _feeling.userUid);
  }

  int findFeelingIndexByDate(DateTime date, int increment) {
    return feelings.indexWhere((feeling) => feeling.date == date && feeling.increment == increment);
  }
}

class FeelingService {
  final Firestore _db = Firestore.instance;
  var unsubscribe;

  subscribeToDB(context) {
    if (unsubscribe != null) return;

    Profile profile = Provider.of<AuthState>(context).profile;
    FeelingState feelingState = Provider.of<FeelingState>(context);
    var ref = _db.collection("feelings").where("userUid", isEqualTo: profile.uid);

    unsubscribe = ref.snapshots().listen((snapshot) {
      snapshot.documentChanges.forEach((change) {
        var data = change.document.data;
        Feeling feeling = Feeling();
        feeling.userUid = data['userUid'];
        feeling.mood = Mood.values.firstWhere((mood) => mood.toString() == data['mood']);
        feeling.date = data['date'].toDate();
        feeling.increment = data['increment'];
        feeling.updated = data['updated'].toDate();

        if (change.type == DocumentChangeType.added) {
          feelingState.addFeeling(feeling);
        } else if (change.type == DocumentChangeType.modified) {
          feelingState.updateFeeling(feeling);
        } else if (change.type == DocumentChangeType.removed) {
          feelingState.removeFeeling(feeling);
        }
      });
    });
  }

  unsubscribeFromDB() {
    if (unsubscribe != null) return;
    unsubscribe();
    unsubscribe = null;
  }

  Future<FirebaseUser> updateFeeling(context, mood, date, increment) async {
    FirebaseUser user = Provider.of<AuthState>(context).user;
    FeelingState feelingState = Provider.of<FeelingState>(context);
    feelingState.setLoading(true);

    Feeling feeling = Feeling();
    feeling.userUid = user.uid;
    feeling.mood = mood;
    feeling.date = date;
    feeling.increment = increment;

    updateDatabase(feeling);

    feelingState.setLoading(false);
    print("[HEAT] updated feeling: " + feeling.toString());
    return user;
  }

  Future<void> updateDatabase(Feeling feeling) async {
    String docId = '${feeling.date}-${feeling.increment}-${feeling.userUid}';
    DocumentReference ref = _db.collection('feelings').document(docId);
    DocumentSnapshot snapshot = await ref.get();

    if (feeling.mood != null && (snapshot.data == null || feeling.mood.toString() != snapshot.data['mood'])) {
      ref.setData({
        'userUid': feeling.userUid,
        'mood': feeling.mood.toString(),
        'date': feeling.date,
        'increment': feeling.increment,
        'updated': DateTime.now(),
      }, merge: true);
    } else {
      ref.delete();
    }
  }

  Color getColor(Mood mood) {
    switch (mood) {
      case Mood.happy:
        return Colors.green;
      case Mood.neutral:
        return Colors.yellow;
      case Mood.sad:
        return Colors.red;
    }

    return Colors.grey;
  }

  charts.Color getChartColor(Feeling _feeling, DateTime selectedDate) {
    var color;
    switch (_feeling.mood) {
      case Mood.happy:
        color = charts.MaterialPalette.green.shadeDefault;
        break;
      case Mood.neutral:
        color = charts.MaterialPalette.yellow.shadeDefault;
        break;
      case Mood.sad:
        color = charts.MaterialPalette.red.shadeDefault;
        break;
    }

    int opacity = _feeling.date.day == selectedDate.day ? 170 : 50;
    return new charts.Color(r: color.r, g: color.g, b: color.b, a: opacity);
  }
}

final FeelingService feelingService = FeelingService();
