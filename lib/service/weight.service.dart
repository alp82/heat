import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'auth.service.dart';

class Weight {
  String userUid;
  double amount;
  DateTime date;
  DateTime updated;
}

class WeightState with ChangeNotifier {
  bool loading = false;
  List<Weight> weights = [];

  void setLoading(bool _loading) {
    loading = _loading;
    notifyListeners();
  }

  void setWeights(List<Weight> _weights) {
    weights = _weights;
    sortWeightsByDate();
    notifyListeners();
  }

  void addWeight(Weight _weight) {
    weights.add(_weight);
    sortWeightsByDate();
    notifyListeners();
  }

  void updateWeight(Weight _weight) {
    int weightIndex = findWeightIndex(_weight);
    if (weightIndex >= 0) {
      weights[weightIndex] = _weight;
      sortWeightsByDate();
      notifyListeners();
    }
  }

  void removeWeight(Weight _weight) {
    int weightIndex = findWeightIndex(_weight);
    if (weightIndex >= 0) {
      weights.removeAt(weightIndex);
      notifyListeners();
    }
  }

  void sortWeightsByDate() {
    weights.sort((a, b) {
      return a.date.compareTo(b.date);
    });
  }

  int findWeightIndex(Weight _weight) {
    return weights.indexWhere((weight) => weight.date == _weight.date && weight.userUid == _weight.userUid);
  }

  int findWeightIndexByDate(DateTime date) {
    return weights.indexWhere((weight) => weight.date == date);
  }
}

class WeightService {
  final Firestore _db = Firestore.instance;
  var unsubscribe;

  subscribeToDB(context) {
    if (unsubscribe != null) return;

    Profile profile = Provider.of<AuthState>(context).profile;
    WeightState weightState = Provider.of<WeightState>(context);
    var ref = _db.collection("weights").where("userUid", isEqualTo: profile.uid);

    unsubscribe = ref.snapshots().listen((snapshot) {
      snapshot.documentChanges.forEach((change) {
        var data = change.document.data;
        Weight weight = Weight();
        weight.userUid = data['userUid'];
        weight.amount = data['amount'];
        weight.date = data['date'].toDate();
        weight.updated = data['updated'].toDate();

        if (change.type == DocumentChangeType.added) {
          weightState.addWeight(weight);
        } else if (change.type == DocumentChangeType.modified) {
          weightState.updateWeight(weight);
        } else if (change.type == DocumentChangeType.removed) {
          weightState.removeWeight(weight);
        }
      });
    });
  }

  unsubscribeFromDB() {
    if (unsubscribe != null) return;
    unsubscribe();
    unsubscribe = null;
  }

  Future<FirebaseUser> updateWeight(context, amount, date) async {
    FirebaseUser user = Provider.of<AuthState>(context).user;
    WeightState weightState = Provider.of<WeightState>(context);
    weightState.setLoading(true);

    Weight weight = Weight();
    weight.userUid = user.uid;
    weight.amount = double.parse(amount, (e) => null);
    weight.date = date;

    updateDatabase(weight);

    weightState.setLoading(false);
    print("[HEAT] updated weight: " + weight.toString());
    return user;
  }

  void updateDatabase(Weight weight) {
    String docId = '${weight.date}-${weight.userUid}';
    DocumentReference ref = _db.collection('weights').document(docId);

    if (weight.amount != null) {
      ref.setData({
        'userUid': weight.userUid,
        'amount': weight.amount,
        'date': weight.date,
        'updated': DateTime.now(),
      }, merge: true);
    } else {
      ref.delete();
    }
  }
}

final WeightService weightService = WeightService();
