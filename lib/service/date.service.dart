import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

DateTime now = new DateTime.now();
DateTime today = new DateTime(now.year, now.month, now.day);

class DateState with ChangeNotifier {
  DateTime selectedDay = today;

  void setSelectedDay(_selectedDay) {
    selectedDay = _selectedDay;
    notifyListeners();
  }

  void setToday() {
    selectedDay = today;
    notifyListeners();
  }

  void setPreviousDay() {
    DateTime previousDay = selectedDay.subtract(new Duration(hours: 23));
    selectedDay = new DateTime(previousDay.year, previousDay.month, previousDay.day);
    notifyListeners();
  }

  void setNextDay() {
    DateTime nextDay = selectedDay.add(new Duration(hours: 25));
    selectedDay = new DateTime(nextDay.year, nextDay.month, nextDay.day);
    notifyListeners();
  }
}

class DateService {
  bool isToday(DateTime date) {
    return date.year == today.year && date.month == today.month && date.day == today.day;
  }

  bool isYesterday(DateTime date) {
    return date.year == today.year && date.month == today.month && date.day == today.day - 1;
  }

  bool isLastWeek(DateTime date) {
    Duration difference = today.difference(date);
    return difference.inDays <= 7;
  }

  String getDateName(DateTime date) {
    if (isToday(date)) {
      return 'Today';
    }

    if (isYesterday(date)) {
      return 'Yesterday';
    }

    String prefix = isLastWeek(date) ? 'Last ' : '';
    return prefix + DateFormat('EEEE').format(date);
  }
}

final DateService dateService = DateService();
