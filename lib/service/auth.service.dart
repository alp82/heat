import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:provider/provider.dart';

class Profile {
  String uid;
  String displayName;
  String email;
  String photoUrl;
  DateTime lastSeen;
}

class AuthState with ChangeNotifier {
  bool loading = false;
  FirebaseUser user;
  Profile profile;

  void setLoading(_loading) {
    loading = _loading;
    notifyListeners();
  }

  void setUser(_user) {
    user = _user;
    notifyListeners();
  }

  void setProfile(_profile) {
    profile = _profile;
    notifyListeners();
  }
}

class AuthService {
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final Firestore _db = Firestore.instance;

  Future<FirebaseUser> googleSignIn(context) async {
    AuthState authState = Provider.of<AuthState>(context);
    authState.setLoading(true);

    GoogleSignInAccount googleUser = await _googleSignIn.signIn();

    GoogleSignInAuthentication googleAuth = await googleUser.authentication;
    AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );
    FirebaseUser user = (await _auth.signInWithCredential(credential)).user;

    updateDatabase(user).listen((profile) {
      authState.setUser(user);
      authState.setProfile(profile);
      authState.setLoading(false);
      print("[HEAT] signed in with google: " + user.displayName);
    });

    return user;
  }

  Stream<Profile> updateDatabase(FirebaseUser user) {
    DocumentReference ref = _db.collection('users').document(user.uid);

    ref.setData({
      'uid': user.uid,
      'email': user.email,
      'displayName': user.displayName,
      'photoUrl': user.photoUrl,
      'lastSeen': DateTime.now(),
    }, merge: true);

    return ref.snapshots().map((snap) => snap.data).map((data) {
      Profile profile = Profile();
      profile.uid = data['uid'];
      profile.email = data['email'];
      profile.displayName = data['displayName'];
      profile.photoUrl = data['photoUrl'];
      profile.lastSeen = data['lastSeen'].toDate();
      return profile;
    });
  }

  void signOut(context) {
    _auth.signOut();
    AuthState authState = Provider.of<AuthState>(context);
    authState.setUser(null);
    authState.setProfile(null);
  }
}

final AuthService authService = AuthService();
