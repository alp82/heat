import 'package:flutter/material.dart';
import 'package:heat_app/service/feeling.service.dart';
import 'package:provider/provider.dart';

class FeelingForm extends StatefulWidget {
  final DateTime date;

  FeelingForm(this.date);

  @override
  FeelingFormState createState() {
    return FeelingFormState();
  }
}

class FeelingFormState extends State<FeelingForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    FeelingState feelingState = Provider.of<FeelingState>(context);
    int feelingIndex = feelingState.findFeelingIndexByDate(widget.date, 0);

    Feeling feeling;
    if (feelingIndex >= 0) {
      feeling = feelingState.feelings[feelingIndex];
    }

    return Form(
        key: _formKey,
        child: Row(
          children: <Widget>[
            MoodIcon(feeling, Mood.happy, () {
              feelingService.updateFeeling(context, Mood.happy, widget.date, 0);
            }),
            MoodIcon(feeling, Mood.neutral, () {
              feelingService.updateFeeling(context, Mood.neutral, widget.date, 0);
            }),
            MoodIcon(feeling, Mood.sad, () {
              feelingService.updateFeeling(context, Mood.sad, widget.date, 0);
            }),
          ],
        ));
  }
}

class MoodIcon extends StatefulWidget {
  final Feeling feeling;
  final Mood mood;
  final Function onSelect;

  MoodIcon(this.feeling, this.mood, this.onSelect);

  @override
  MoodIconState createState() {
    return MoodIconState();
  }
}

class MoodIconState extends State<MoodIcon> {
  @override
  Widget build(BuildContext context) {
    IconData icon;
    Color color = feelingService.getColor(widget.mood);
    switch (widget.mood) {
      case Mood.happy:
        icon = Icons.sentiment_very_satisfied;
        color = Colors.green;
        break;
      case Mood.neutral:
        icon = Icons.sentiment_neutral;
        color = Colors.yellow;
        break;
      case Mood.sad:
        icon = Icons.sentiment_very_dissatisfied;
        color = Colors.red;
        break;
    }

    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10),
      child: IconButton(
        icon: Icon(
          icon,
          color: widget.feeling != null && widget.feeling.mood == widget.mood ? color : Colors.grey,
          size: 40,
        ),
        tooltip: widget.mood.toString(),
        onPressed: widget.onSelect,
      ),
    );
  }
}
