import 'package:flutter/material.dart';
import 'package:heat_app/service/weight.service.dart';
import 'package:provider/provider.dart';

class WeightForm extends StatefulWidget {
  final DateTime date;

  WeightForm(this.date);

  @override
  WeightFormState createState() {
    return WeightFormState();
  }
}

class WeightFormState extends State<WeightForm> {
  final _formKey = GlobalKey<FormState>();
  final weightController = TextEditingController();

  @override
  void dispose() {
    weightController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    WeightState weightState = Provider.of<WeightState>(context);
    int weightIndex = weightState.findWeightIndexByDate(widget.date);

    String inputValue = '';
    if (weightIndex >= 0) {
      inputValue = weightState.weights[weightIndex].amount.toString() ?? '';
    }

    weightController.text = inputValue;

    return Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 150.0,
              child: TextFormField(
                controller: weightController,
                keyboardType: TextInputType.number,
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 40),
                decoration: const InputDecoration(
                  border: const OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                  ),
                  hintText: '?',
                  suffixText: 'kg',
                  suffixStyle: const TextStyle(
                    color: Colors.grey,
                    fontSize: 20,
                  ),
                ),
                validator: (String value) {
                  double isDouble = double.parse(value, (e) => null);
                  if (isDouble != null || value == '')
                    return null;
                  else
                    return 'Only numbers';
                },
              ),
            ),
            Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: RaisedButton(
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      weightService.updateWeight(context, weightController.text, widget.date);
                      Scaffold.of(context).showSnackBar(SnackBar(content: Text('Saved')));
                    }
                  },
                  child: Text(
                    'Save Weight',
                    textScaleFactor: 1.5,
                  ),
                )),
          ],
        ));
  }
}
